import java.util.Optional;

/**
 * A class responsible for locking states. This is where you can implement
 * a proxy between your transaction and its resource acquisition!
 * 
 * @param T The state's class.
 */
class LockStateService<T> {
    
    private final StateRepository<T> stateRepository;

    public LockStateService(StateRepository<T> stateRepository) {
        this.stateRepository = stateRepository;
    }

    public Lock<T> acquire(T state) {
        /**
         * Create a shared state. This state is said to be shared because its value
         * must be the same accross any instances connecting to the same data repository.
         */
        Optional<T> sharedState = Optional.empty();

        /**
         * This section is responsible to retry acquiring the lock. Here, 
         * the programmer can implement whichever policy that better fits his 
         * requirements. For instance, this section will retry infinitely and 
         * as fast as possible to acquire the state.
         * 
         * You can do differently: maybe implement a limited amount of retries,
         * a timed executor etc.
         */
        while (!sharedState.isPresent()) {
            sharedState = Optional.ofNullable(attemptLockAcquisition(state));
        }

        /**
         * If the process could lock the state, go ahead and return it to the
         * transaction scope. Otherwise, throw an exception (even though this exception
         * will not throw for the code implemented above!)
         */
        return sharedState.map(Lock::new).orElseThrow(LockAcquisitionException::new);
    }

    protected T attemptLockAcquisition(T state) {
        try {
            return stateRepository.create(state);
        } catch (LockIsInUseException lockIsInUseException) {
            return null;
        } catch (Exception exception) {
            throw exception;
        }
    }

    public void release(Lock<T> lockedState) {
        stateRepository.remove(lockedState.state());
    }
}
