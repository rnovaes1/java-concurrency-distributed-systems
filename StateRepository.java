
/**
 * An interface for a generic state repository. The operations described here MUST
 * be ATOMIC, CONSISTENT, ISOLATED and DURABLE, in the terms of distributed systems.
 * 
 * Good candidates are:
 * 
 * - Single-instance Web servers with synchronous REST APIs enabled;
 * - Any database systems that support transactions (at least in insertion level).
 * 
 * @param T A class whose objects' states should be considered candidates
 * for race conditions.
 */
interface StateRepository<T> {

    /**
     * Atomic operation to create a single and distributed state object at the data repository.
     * If one process tries to insert a repeated state, then an exception should be thrown.
     * 
     * @param state The state to be persisted atomically in the data repository.
     * @see LockInUseException An example class to be caught when the state can't be locked.
     */
    T create(T state);

    /**
     * ~Atomic operation~ to remove a single and distributed state object from the data 
     * repository.
     * 
     * This operation has no critical need to be atomic, as long as consumers can
     * accept some threads waiting longer to be finished. This can happen on
     * data repositories that support soft-state, like MongoDB. 
     * 
     * @param state The state to be removed from the data repository.
     */
    void remove(T state);
}
