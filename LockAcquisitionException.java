class LockAcquisitionException extends RuntimeException {
    
    public LockAcquisitionException() {
        super("A fatal error has occurred and the process could not access the desired resource. Aborting!");
    }
}
