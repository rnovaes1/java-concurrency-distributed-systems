class LockIsInUseException extends RuntimeException {

    public LockIsInUseException() {
        super("There is one process or application using the resource you're trying to access." + 
            " Please be patient and wait!");
    }
    
}
