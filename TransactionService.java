import java.util.function.Consumer;

/**
 * A class that implements a transaction such that no race condition is
 * expected for concurrent operations. Two operations are said to be
 * concurrent whenever their states are equal.
 * 
 * @param T The state which will synchronize your transactions.
 */
class TransactionService<T> {

    private final LockStateService<T> lockStateService;

    public TransactionService(LockStateService<T> lockStateService) {
        this.lockStateService = lockStateService;
    }

    public void doInTransaction(T state, Consumer<T> concurrentOperation) {
        try {
            Lock<T> acquiredLock = lockStateService.acquire(state);
            
            doInTransactionWhenLockIsAcquired(acquiredLock, concurrentOperation);
        } catch (Exception exception) {
            /**
             * Any exception during commit or rollback flows? Maybe you'd like
             * to catch them here.
             */
        }
    }

    /**
     * If the lock is acquired, the following code will run only in this process while
     * other processes, whose `acquiredLock.state()` is equivalent to this one, will
     * have to wait.
     * 
     * Hence, you can consider this method will follow ACID guidelines as long as 
     * `concurrentOperations` also follows them. However, we do guarantee that no
     * race conditions will occur, as well as that every resource acquired will
     * be released.
     */
    protected void doInTransactionWhenLockIsAcquired(Lock<T> acquiredLock, Consumer<T> concurrentOperation) {
        try {
            concurrentOperation.accept(acquiredLock.state());
            commit(acquiredLock);
        } catch (Exception exception) {
            rollback(acquiredLock, exception);
        } finally {
            lockStateService.release(acquiredLock);
        }
    }

    /**
     * Implement your commit operations.
     */
    protected void commit(Lock<T> acquiredLock) {
        
    }

    /**
     * Implement your rollback operations.
     */
    protected void rollback(Lock<T> acquiredLock, Exception exception) {

    }
}
